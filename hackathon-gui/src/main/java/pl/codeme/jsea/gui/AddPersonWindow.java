package pl.codeme.jsea.gui;

import java.util.ArrayList;
import java.util.List;

import javafx.event.Event;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pl.codeme.jsea.client.Client;
import pl.codeme.jsea.database.model.Person;

/** Class for javafx window where a user sets data of new person that he wants to add to xml file on server. 
 * 
 * @author malgorzata.syska@o2.pl
 */
public class AddPersonWindow extends Stage {
	
	protected static String communicateFromServer;
	
	public AddPersonWindow(Stage parent, Client client, TextArea detailsTextArea, Button refreshButton) {
		
		parent.hide();
		AddPersonWindow window = this;
		window.show();
		this.setTitle("Dodawanie nowej osoby");

		VBox rootNode = new VBox();
		rootNode.setAlignment(Pos.TOP_CENTER);
	
		Label nameLabel = new Label("imię");
		Label surnameLabel = new Label("nazwisko");
		Label phoneNumberLabel = new Label("numer telefonu");
		Label cityLabel = new Label("miasto");
		Label heightLabel = new Label("wzrost");
		List<Label> labels = new ArrayList<>();
		labels.add(nameLabel);
		labels.add(surnameLabel);
		labels.add(phoneNumberLabel);
		labels.add(cityLabel);
		labels.add(heightLabel);
	
		TextField nameField = new TextField();
		TextField surnameField = new TextField();
		TextField phoneNumberField = new TextField();
		TextField cityField = new TextField();
		TextField heightField = new TextField();
		List<TextField> textFields = new ArrayList<>();
		textFields.add(nameField);
		textFields.add(surnameField);
		textFields.add(phoneNumberField);
		textFields.add(cityField);
		textFields.add(heightField);
		
		for (int i = 0;i<labels.size();i++) {
			rootNode.getChildren().add(labels.get(i));
			rootNode.getChildren().add(textFields.get(i));
		}
	
		Button returnButton = new Button("Powrót");
		rootNode.getChildren().add(returnButton);
    
		@SuppressWarnings("unused")
		ButtonHandlerInterface buttonHandler;
    
		returnButton.setOnMouseClicked(buttonHandler = (event) -> {
			window.close();
			parent.show();
		});
    
		Button saveButton = new Button("Zapisz");
		rootNode.getChildren().add(saveButton);
		saveButton.setOnMouseClicked(buttonHandler = (event) 
				-> tryAddNewPerson(event, textFields, client, window, parent, detailsTextArea, refreshButton));
    
		Scene scene = new Scene(rootNode, 400, 400);
		this.setScene(scene);
	}
	
	/** It pops up Alert window with communicate when not all fields of person's data were filled. 
	 */
	private void unacceptableData() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Puste pole");
		alert.setContentText("Wszystkie pola muszą być wypełnione!");
		alert.showAndWait();
	}
	
	/** It pops up Alert window with communicate when wrong format of height of person was filled. 
	 */
	private void wrongHeight() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Niepoprawny format danych");
		alert.setContentText("Podałeś niepoprawny format wzrostu. Podaj liczbę.");
		alert.showAndWait();
	}
	
	/** It is called when an user wants to save new person in file on server. It checks if all data filled by user is correct. 
	 * It creates new instance of person, fills it with data given by user and saves it in file on server. 
	 * 
	 * @param event - mouse clicked
	 * @param textFields
	 * @param client
	 * @param window
	 * @param parent
	 * @param detailsTextArea
	 * @param refreshButton
	 */
	private void tryAddNewPerson(MouseEvent event, List<TextField> textFields, Client client, 
			AddPersonWindow window, Stage parent, TextArea detailsTextArea, Button refreshButton) {
		String newName = textFields.get(0).getText().trim();
		String newSurname = textFields.get(1).getText().trim();
		String newPhoneNumber = textFields.get(2).getText().trim();
		String newCity = textFields.get(3).getText().trim();
		String newHeight = textFields.get(4).getText().trim();
		
		try {
			Integer.valueOf(newHeight);
		} catch (NumberFormatException e) {
			wrongHeight();
		}
		
		if (newName.length()==0 || newSurname.length()==0 || newPhoneNumber.length()==0 ||
				newCity.length()==0 || newHeight.length()==0) {
			unacceptableData();
		}
		else {
			Person person = new Person(0,newName,newSurname,newPhoneNumber,newCity,Integer.valueOf(newHeight));
			String communicateFromServer = client.addPerson(person);
			System.out.println("communicateFromServer : " + communicateFromServer);
	    	window.close();
	    	parent.show(); 
			Event.fireEvent(refreshButton, event);
			detailsTextArea.setText(communicateFromServer);
		}
	}
}
