package pl.codeme.jsea.gui;

import java.util.List;
import java.util.Optional;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pl.codeme.jsea.client.Client;
import pl.codeme.jsea.database.model.Person;

/** Main window of graphical user interface. It displays list of all persons from file saved on server and provides several options such as:
 * - add a new person (new window is opened)
 * - delete a person
 * - show details of a person
 * - refresh list
 * - close an application
 * 
 * @author malgorzata.syska@o2.pl
 */
public class MainWindow extends Application {
	
	protected Client client = new Client("localhost", 6565);
	private ListView<Person> listOfPersonsListView;
	
    public static void main( String[] args )     {
        launch(args);
    }

	@Override
	public void start(Stage stage) throws Exception {
		
		VBox mainPane = new VBox();
		mainPane.setPrefSize(480, 500);

		GridPane listPane = new GridPane();
		GridPane buttonPane = new GridPane();
		mainPane.getChildren().add(listPane);
		mainPane.getChildren().add(buttonPane);
		listPane.setPadding(new Insets(10, 10, 10, 10));
		buttonPane.setPadding(new Insets(10, 10, 10, 10));
		listPane.setAlignment(Pos.TOP_CENTER);
		buttonPane.setAlignment(Pos.TOP_CENTER);
		listPane.setHgap(10);
		buttonPane.setHgap(10);
		buttonPane.setVgap(10);
		
		Scene scene = new Scene(mainPane,480,500);
		stage.setScene(scene);
		stage.show();
		stage.setTitle("Baza osób");
		                                           
		listOfPersonsListView = new ListView<>();
		listPane.getChildren().add(listOfPersonsListView);
		GridPane.setConstraints(listOfPersonsListView, 0, 0);
		GridPane.setValignment(listOfPersonsListView, VPos.TOP);
		listOfPersonsListView.setPrefSize(250, 400);
		showAllPersonsRequest();
		listOfPersonsListView.getSelectionModel().select(0);
		
		TextArea detailsTextArea = new TextArea();
		detailsTextArea.setPrefSize(200, 200);
		listPane.getChildren().add(detailsTextArea);
		GridPane.setConstraints(detailsTextArea, 1, 0);
		GridPane.setValignment(detailsTextArea, VPos.TOP);
		detailsTextArea.setEditable(false);
		detailsTextArea.setPrefRowCount(4);
		
		Button detailsButton = new Button("Szczegóły");
		Button refreshButton = new Button("Odśwież");
		Button addPersonButton = new Button("Dodaj");
		Button deletePersonButton = new Button("Usuń");
		Button closeButton = new Button("Koniec");
		
		buttonPane.getChildren().add(detailsButton);
		buttonPane.getChildren().add(refreshButton);
		buttonPane.getChildren().add(addPersonButton);
		buttonPane.getChildren().add(deletePersonButton);
		buttonPane.getChildren().add(closeButton);
		
		GridPane.setConstraints(detailsButton, 0, 0);
		GridPane.setConstraints(refreshButton, 1, 0);
		GridPane.setConstraints(addPersonButton, 0, 1);		
		GridPane.setConstraints(deletePersonButton, 1, 1);
		GridPane.setConstraints(closeButton, 0, 2);
		
		detailsButton.setPrefWidth(100);
		refreshButton.setPrefWidth(100);
		addPersonButton.setPrefWidth(100);
		deletePersonButton.setPrefWidth(100);
		closeButton.setPrefWidth(100);
		
		
		@SuppressWarnings("unused")
		ButtonHandlerInterface buttonHandler;
		
		detailsButton.setOnMouseClicked(buttonHandler = (event) -> {
			Person person = null;
			Person p = null;
			try {
				person = listOfPersonsListView.getSelectionModel().getSelectedItem();
				p = client.findOnePerson(person.getId());
			} catch (Exception e) {
				detailsTextArea.setText("Nie wybrano osoby");
				return;
			}
			
			if(p.getId()==0) {
				detailsTextArea.setText(p.getName().toString());
				return;
			}
			
			StringBuffer fullData = new StringBuffer("");
			fullData.append(p.getName() + " " + p.getSurname()+ "\nnumer: " + p.getPhoneNumber() + "\nmiasto: " 
			+ p.getCity() + "\nwzrost: " + p.getHeight());

			detailsTextArea.setText(fullData.toString());
		});
		
		refreshButton.setOnMouseClicked(buttonHandler = (event) -> {
			showAllPersonsRequest();
			detailsTextArea.setText("");
		});
		
		addPersonButton.setOnMouseClicked(buttonHandler = (event) -> {
			new AddPersonWindow(stage, client, detailsTextArea, refreshButton);
		});
		
		deletePersonButton.setOnMouseClicked(buttonHandler = (event) -> {
			if (!confirmDeletingPerson()) return;
			Person person = listOfPersonsListView.getSelectionModel().getSelectedItem();
			String communicate = client.deletePerson(person.getId());
			Event.fireEvent(refreshButton, event);
			detailsTextArea.setText(communicate);
		});
		
		closeButton.setOnMouseClicked(buttonHandler = (event) -> System.exit(0));
		
	}
	
	/** Refreshes a list of persons.  
	 */
	private void showAllPersonsRequest() {
		List<Person> listOfAllPersons = client.showAllPersons();
		ObservableList<Person> data = FXCollections.observableArrayList();
		for(Person person : listOfAllPersons) {
			data.add(person);
		}
		listOfPersonsListView.setItems(data);
		listOfPersonsListView.refresh();
		listOfPersonsListView.getSelectionModel().select(0);
	}
	
	/** Before an user can delete a person from list, an alert window pops up and an user has to confirm deleting a person. 
	 * 
	 * @return true if an user confirmed deleting a person from list or false if an user didn't confirmed deleting a person from list
	 */
	private boolean confirmDeletingPerson () {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Potwierdzenie usunięcia osoby");
		alert.setContentText("Czy jesteś pewien, że chcesz usunąć tą osobę? Operacji nie da się cofnąć!");
		Optional<ButtonType> opcja = alert.showAndWait();
		if (opcja.get().getButtonData().isCancelButton()) return false;
		return true;
	}
}
