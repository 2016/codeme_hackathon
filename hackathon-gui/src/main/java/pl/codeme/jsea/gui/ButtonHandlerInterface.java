package pl.codeme.jsea.gui;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/** Functional interface created for handling button actions using lambda expression. 
 * 
 * @author malgorzata.syska@o2.pl
 */
public interface ButtonHandlerInterface extends EventHandler<MouseEvent> {

	@Override
	public void handle(MouseEvent event);
	
}
