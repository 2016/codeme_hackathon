package pl.codeme.jsea.client;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import pl.codeme.jsea.database.model.Person;
import pl.codeme.jsea.server.Statement;

/** Class that parses response from server to client. Depends on used action by client ("find", "all", "add", "del"), 
 * there are three corresponding methods and CRC-check.
 * 
 * @author malgorzata.syska@o2.pl
 */
public class ParsingResponseFromServer {
	
	Statement responseFromServer;		// whole statement given by server
	
	/** Constructor
	 * 
	 * @param responseFromServer whole statement given by server
	 */
	public ParsingResponseFromServer(Statement responseFromServer) {
		this.responseFromServer = responseFromServer;
	}
	
	/** Method that parses response from server if client asked for all persons. 
	 * It checks CRC (checksum) from server - when it's wrong - it sets communicate about wrong checksum. 
	 * If CRC from server is correct, it tries to get data about all persons on list - name, surname, phoneNumber, city and height. 
	 * If these operations throw JSONException - it gets message from error field in response from server. 
	 * 
	 * @return list of all persons on list
	 */	
	public List<Person> parseResponseFromServerAllPersons() {
		List<Person> listOfDetails = new ArrayList<>();
		Person person = null;

		try {
				
			if(!checkCRCFromServer(responseFromServer)) {
				listOfDetails.add(new Person(0,"Błąd w sumie kontrolnej","","","",0));
			}
			else {
				for (int i = 0;i<responseFromServer.getData().getJSONArray("osoba").length(); i++) {
					int id = responseFromServer.getData().getJSONArray("osoba").getInt(i);
					String name = responseFromServer.getData().getJSONArray("name").getString(i);
					String surname = responseFromServer.getData().getJSONArray("surname").getString(i);
					String phoneNumber = responseFromServer.getData().getJSONArray("phoneNumber").getString(i);
					String city = responseFromServer.getData().getJSONArray("city").getString(i);
					int height = responseFromServer.getData().getJSONArray("height").getInt(i);
					person = new Person(id,name,surname,phoneNumber,city,height);
					listOfDetails.add(person);
				}
			}
		} catch (JSONException e) {
			listOfDetails.add(new Person(0,responseFromServer.getData().getString("error"),"","","",0));
		}

		return listOfDetails;
	}

	/** Method that parses response from server if client asked for one person. 
	 * It checks CRC (checksum) from server - when it's wrong - it sets communicate about wrong checksum. 
	 * If CRC from server is correct, it tries to get data about chosen persons - name, surname, phoneNumber, city and height. 
	 * If there operations throw JSONException - it gets message from error field in response from server. 
	 * 
	 * @return Person concrete person
	 */
	public Person parseResponseFromServerFindPerson() {
		System.out.println(responseFromServer.getStatement());
		Person person = null;
		
		try {
			if(!checkCRCFromServer(responseFromServer)) {
				person = new Person(0,"Błąd w sumie kontrolnej.","","","",0);
			}
			else {
				int id = responseFromServer.getData().getInt("osoba");
				String name = responseFromServer.getData().getString("name");
				String surname = responseFromServer.getData().getString("surname");
				String phoneNumber = responseFromServer.getData().getString("phoneNumber");
				String city = responseFromServer.getData().getString("city");
				int height = responseFromServer.getData().getInt("height");
				person = new Person(id,name,surname,phoneNumber,city,height);
			}
		} catch (JSONException e) {
			person = new Person(0,responseFromServer.getData().getString("error"),"","","",0);
		}
	
		return person;
	}

	/** Method that parses response from server if client asked for adding or deleting person. 
	 * It checks CRC (checksum) from server - when it's wrong - it sets communicate about wrong checksum. 
	 * If CRC from server is correct, it tries to get communicate from server whether a person was added/deleted. 
	 * 
	 * @return String communicate whether person was added/deleted; if any operation throw exception - 
	 * it returns message from this exception. 
	 */
	public String parseResponseFromServerAddOrDeletePerson() {
		try {
			if(!checkCRCFromServer(responseFromServer)) {
				return "Błąd w sumie kontrolnej.";
			}
			else {
				System.out.println(responseFromServer.getStatement());
				System.out.println("response.getData().getString(\"komunikat\")" + responseFromServer.getData().getString("komunikat"));
				return responseFromServer.getData().getString("komunikat");
			} 
		} catch (Exception e) {
			return e.getMessage();
		} 
	}
	
	/** Method checks correctness of checksum. Checksum - it is hashed value of data sent by server to client. 
	 * It gets checksum from statement given by server (field named crc) and compares it to checksum 
	 * generated on data from statement given by server. 
	 * 
	 * @param responseFromServer response from server in Statement form
	 * @return true if checksum is correct and false if checksum is not correct
	 */
	public boolean checkCRCFromServer(Statement responseFromServer) {
        int crcFromServer = Integer.valueOf(responseFromServer.getCrc());
        System.out.println("crcFromServer : " + crcFromServer);
        JSONObject data = responseFromServer.getData();
        System.out.println("dane : " + data.toString());
        int crcProper = responseFromServer.hash(data);
        System.out.println("crcProper : " + crcProper);
        if(crcFromServer==crcProper) {
        	System.out.println("crc ok!");
        	return true;
        }
        else {
        	return false;
        }
	}

}
