package pl.codeme.jsea.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.List;

import org.json.JSONObject;

import pl.codeme.jsea.database.model.Person;
import pl.codeme.jsea.server.Responses;
import pl.codeme.jsea.server.Statement;
import pl.codeme.jsea.server.actions.Actions;

/** Client class. A client has fields: (String) host, (int) port, (ParsingResponseFromServer) 
 * parsedResponseFromServer and (String) actionGivenByClient.
 * 
 * @author k.kuczynska@developers.horde.pl
 * @author maciek.stepniak@gmail.com
 * @author malgorzata.syska@o2.pl
 */
public class Client {
	private String host;
	private int port;	// default 6565
	private ParsingResponseFromServer parsedResponseFromServer;
	private String actionGivenByClient;

	/** Constructor.
	 * 
	 * @param host host of connection
	 * @param port port of connection, default 6565
	 */
	public Client(String host, int port) {
		super();
		this.host = host;
		this.port = port;
	}

	/** Overloaded method. Used when no String parameter was given.
	 * @param actionStr action in String form
	 * @return send method with default String argument.
	 */
	public Statement send(String actionStr) {
		return send(actionStr, new String[0]);
	}

	/** Method responsible for sending request to server and receiving answer. 
	 * In try-catch block it opens new socket connection, buffered reader and buffered writer for sending and receiving message. 
	 * It checks what action was chosen by user and puts it in request. Then it receives an answer. 
	 * 
	 * @param stringNameForActionGivenByUser String name for action given by an user
	 * @param args arguments for action given by an user such as ID of person or full data of a new person
	 * @return Statement response from server
	 */
	public Statement send(String stringNameForActionGivenByUser, String... args) {
		Statement responseFromServer = new Statement();
		try (Socket socket = new Socket(this.host, this.port);
				BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));) {

			Statement requestToServer = new Statement();
			JSONObject dataSentToServer = new JSONObject();

			Actions action = Actions.getAction(stringNameForActionGivenByUser);
			if (action != null) {
				switch (action) {
				case FIND:
					System.out.println("find ID " + args[0]);
					dataSentToServer.put("id", args[0]);
					break;
				case ALL:
					System.out.println("find ALL ");
					dataSentToServer.put("all",0);
					break;
				case ADD:
					System.out.println("add " + args[0]);
					dataSentToServer.put("name", args[0]);
					dataSentToServer.put("surname", args[1]);
					dataSentToServer.put("phoneNumber", args[2]);
					dataSentToServer.put("city", args[3]);
					dataSentToServer.put("height", args[4]);
					break;
				case DEL:
					System.out.println("delete ID " + args[0]);
					dataSentToServer.put("id", args[0]);
					break;
				default:
					break;
				}
			} else {
				responseFromServer = new Statement();
				responseFromServer.setError("Błędna komenda");
				return responseFromServer;
			}

			requestToServer.setAction(Responses.getResponse(action.getLabel()));
			requestToServer.setData(dataSentToServer);

			// sends statement to server
			writer.write(requestToServer.getStatement() + "\n");
			writer.flush();
			System.out.println("Wysłano do serwera: " + requestToServer.getStatement());
			// reads statement from server
			String fromServer = reader.readLine();
			responseFromServer = new Statement(fromServer);
			
			System.out.println("cały statement od serwera " + responseFromServer.getStatement());
			System.out.println("response.getData() z send " + responseFromServer.getData());
			
		} catch (IOException e) {
			responseFromServer.setError(e.getMessage());
		}
		return responseFromServer;
	}

	/** Method connects Client class with GUI. It is called in GUI. 
	 * If user clicks on details button to get more information about chosen person - he calls this method. 
	 * It sets actionGivenByClient to "find", calls send method with "find" action and id of chosen person. 
	 * Then it creates new instance of ParsingResponseFromServer class and calls parsing method 
	 * which parses response from server.
	 * 
	 * @param idOfChosenPerson Id of person chosen by an user when he wants to display details about this person
	 * @return Person chosen by user, which is passed to GUI
	 */
	public Person findOnePerson(int idOfChosenPerson) {
		actionGivenByClient = "find";
		Statement responseFromServer;
		responseFromServer = send(actionGivenByClient, String.valueOf(idOfChosenPerson));
		System.out.println("response in findOnePerson method " + responseFromServer.getStatement());
		parsedResponseFromServer = new ParsingResponseFromServer(responseFromServer);
		return parsedResponseFromServer.parseResponseFromServerFindPerson();
	}
	
	/** Method connects Client class with GUI. It is called in GUI.
	 * It is called when client connects with server and then every time when user wants to refresh list of persons. 
	 * It is also used after adding new person or deleting it. 
	 * It sets actionGivenByClient to "all", calls send method with "all" action. 
	 * Then it creates new instance of ParsingResponseFromServer class and calls parsing method 
	 * which parses response from server.
	 * 
	 * @return List of Person - pieces of information about all persons, whose are passed to GUI
	 */
	public List<Person> showAllPersons() {
		actionGivenByClient = "all";
		Statement responseFromServer;
		responseFromServer = send(actionGivenByClient);
		parsedResponseFromServer = new ParsingResponseFromServer(responseFromServer);
		System.out.println("showAllPersons response " + responseFromServer.getStatement());
		return parsedResponseFromServer.parseResponseFromServerAllPersons();
		
	}
	
	/** Method connects Client class with GUI. It is called in GUI.
	 * It is called when client wants to add a new person to list.
	 * It sets actionGivenByClient to "add", calls send method with "add" action and all attributes of person given in
	 * parameter.
	 * Then it creates new instance of ParsingResponseFromServer class and calls parsing method 
	 * which parses response from server.
	 * 
	 * @param person which user wants to add
	 * @return String with communicate if a person was added to list or wasn't added
	 */
	public String addPerson(Person person) {
		actionGivenByClient = "add";
		Statement responseFromServer;
		responseFromServer = send(actionGivenByClient,person.getName(), person.getSurname(), person.getPhoneNumber(),
				person.getCity(), String.valueOf(person.getHeight()));
		parsedResponseFromServer = new ParsingResponseFromServer(responseFromServer);
		System.out.println("addPerson response " + responseFromServer.getStatement());
		return parsedResponseFromServer.parseResponseFromServerAddOrDeletePerson();
	}
	
	/** Method connects Client class with GUI. It is called in GUI.
	 * It is called when client wants to delete a person from list.
	 * It sets actionGivenByClient to "del", calls send method with "del" action and id of chosen person 
	 * that will be deleted from list. Then it creates new instance of ParsingResponseFromServer class 
	 * and calls parsing method which parses response from server.
	 * 
	 * @param idOfChosenPersonToDelete Id of person that user wants to delete from server
	 * @return String with communicate if a person was deleted from list or wasn't deleted
	 */
	public String deletePerson(int idOfChosenPersonToDelete) {
		actionGivenByClient = "del";
		Statement responseFromServer;
		responseFromServer = send(actionGivenByClient, String.valueOf(idOfChosenPersonToDelete));
		System.out.println("response in deletePerson method " + responseFromServer.getStatement());
		parsedResponseFromServer = new ParsingResponseFromServer(responseFromServer);
		return parsedResponseFromServer.parseResponseFromServerAddOrDeletePerson();
	}

}