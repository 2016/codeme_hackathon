package pl.codeme.jsea.database.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/** Basic class that represents person from .xml file. 
 * Fields: (int) id, which is a unique key; (String) name, (String) surname, (String) phoneNumber, 
 * (String) city and (int) height.
 * 
 * @author maciek.stepniak@gmail.com
 */
@XmlRootElement(name = "person")
@XmlType(propOrder = { "name", "surname", "phoneNumber", "city", "height" })
public class Person {
	int id;
	String name;
	String surname;
	String phoneNumber;
	String city;
	int height;

	/** Empty constructor for the class object. Object can be filled by setters. 
	 */
	public Person() {

	}

	/** Constructor for the class object.
	 * 
	 * @param id id of person
	 * @param name name of person
	 * @param surname surname of person
	 * @param phoneNumber phone number of person
	 * @param city city of person
	 * @param height height of person
	 */
	public Person(int id, String name, String surname, String phoneNumber, String city, int height) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.phoneNumber = phoneNumber;
		this.city = city;
		this.height = height;
	}

	// Setters and Getters
	@XmlAttribute(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	@Override
	public String toString() {
		return getName() + " " + getSurname();
	}

}
