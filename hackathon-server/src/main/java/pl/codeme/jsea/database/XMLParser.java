package pl.codeme.jsea.database;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;

import pl.codeme.jsea.database.model.Person;
import pl.codeme.jsea.database.model.Persons;


/** Class which methods converts xml file into Person objects.
 * 
 * @author malgorzata.syska@o2.pl
 * @author maciek.stepniak@gmail.com
 */
public class XMLParser {
	private String databaseFile;
	private JAXBContext context;
	private static final String ADDRESS_XML = "persons.xml";

	public XMLParser(String databaseFile) throws JAXBException {
		this.databaseFile = databaseFile;
		initialize();
	}

	/** It creates new instance of JAXBContext using Person.class. 
	 * 
	 * @throws JAXBException
	 */
	private void initialize() throws JAXBException {
		context = JAXBContext.newInstance(Persons.class);
	}

	/** It unpack xml file with all persons and converts it into Persons object. 
	 * 
	 * @return list of all persons from xml file
	 * @throws JAXBException when method tries to unmarshall xml file
	 */
	synchronized public ArrayList<Person> getAllPersons() throws JAXBException {
		Unmarshaller unmarshaller = context.createUnmarshaller();
		File xml = new File(this.databaseFile);
		Persons personsFromXML = (Persons) unmarshaller.unmarshal(xml);
		return (ArrayList<Person>) personsFromXML.getPersonsList();
	}

	/** It unpack xml file with all persons, converts it into Persons object, iterates by ID and finds one person. 
	 * If it won't find a person with given ID, it returns null. 
	 * 
	 * @param id of person that you want to find
	 * @return found person or null
	 * @throws JAXBException when method tries to unmarshall xml file
	 */
	synchronized public Person findPerson(int id) throws JAXBException {
		Unmarshaller unmarshaller = context.createUnmarshaller();
		File xml = new File(this.databaseFile);
		Persons personsFromXML = (Persons) unmarshaller.unmarshal(xml);

		Iterator<Person> iterator = personsFromXML.iterator();
		while (iterator.hasNext()) {
			Person p = iterator.next();
			if (p.getId() == id) {
				return p;
			}
		}
		return null;

	}
	
	/** Method adds one new person to xml file. It gives to a person an unique ID. 
	 * 
	 * @param personGivenByUser - that you want to save in xml file
	 * @throws JAXBException when method tries to unmarshall xml file and then marshall it
	 */
	synchronized public void addPersonToList(Person personGivenByUser) throws JAXBException {
		Unmarshaller unmarshaller = context.createUnmarshaller();
		File xml = new File(this.databaseFile);
		Persons personsFromXML = (Persons) unmarshaller.unmarshal(xml);

		Iterator<Person> iterator = personsFromXML.iterator();
		int id = 0;
		while (iterator.hasNext()) {
			Person p = iterator.next();
			if (p.getId()>id){
				id = p.getId();
			}
		}
		id++;
		personGivenByUser.setId(id);
		
		JAXBContext context = JAXBContext.newInstance(Persons.class);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		List<Person> listOfPersonsFromFile = personsFromXML.getPersonsList();
		listOfPersonsFromFile.add(personGivenByUser);
		Persons persons = new Persons();
		persons.setPersonsList(listOfPersonsFromFile);
		m.marshal(persons, new File(ADDRESS_XML));

	}
	
	/** Method deletes one person from xml file. It gets list of all persons, finds a person that you want to delete, 
	 * removes it from list and saves new list of persons to xml file. 
	 * 
	 * @param id of person that you want to delete
	 * @throws JAXBException when method tries to unmarshall xml file and then marshall it
	 */
	synchronized public void deletePersonFromList(int id) throws JAXBException {
		Unmarshaller unmarshaller = context.createUnmarshaller();
		File xml = new File(this.databaseFile);
		Persons personsFromXML = (Persons) unmarshaller.unmarshal(xml);

		List<Person> listOfPersonsFromFile = personsFromXML.getPersonsList();
		for (int i = 0;i<listOfPersonsFromFile.size();i++) {
			if(listOfPersonsFromFile.get(i).getId() == id) {
				listOfPersonsFromFile.remove(i);
			}
		}

		JAXBContext context = JAXBContext.newInstance(Persons.class);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		Persons persons = new Persons();
		persons.setPersonsList(listOfPersonsFromFile);
		m.marshal(persons, new File(ADDRESS_XML));
	}
	
	/** Creates new file with data of persons on server. It's a test.
	 * 
	 * @throws JAXBException then method tries to create new xml file on server and marshall persons into it
	 * @throws PropertyException -
	 */
	public static void createXML() throws JAXBException, PropertyException {
		System.out.println("Test aplikacji");

		Person person1 = new Person(1, "Michal", "Bigosinski", "123456", "Gdynia", 182);
		Person person2 = new Person(2, "Michalina", "Bigosinska", "654321", "Gdansk", 162);

		List<Person> personsList = new ArrayList<Person>();
		personsList.add(person2);
		personsList.add(person1);

		JAXBContext context = JAXBContext.newInstance(Persons.class);

		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		Persons persons = new Persons();
		persons.setPersonsList(personsList);

		m.marshal(persons, System.out);
		m.marshal(persons, new File(ADDRESS_XML));
	}
	
	
}
