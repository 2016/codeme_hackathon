package pl.codeme.jsea.database.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/** Superior class to "Person", which binds all objects of that class. 
 * It has one field, that stores list of Person-type objects.
 * 
 * @author maciek.stepniak@gmail.com
 */
@XmlRootElement(name = "persons")
@XmlSeeAlso(Person.class)
public class Persons implements Iterable<Person> {
	private List<Person> persons;

	@XmlElements(@XmlElement(name = "person"))
	public List<Person> getPersonsList() {
		return persons;
	}

	public void setPersonsList(ArrayList<Person> persons) {
		this.persons = persons;
	}

	public void setPersonsList(List<Person> personsList) {
		this.persons = personsList;
	}

	@Override
	public Iterator<Person> iterator() {
		return persons.iterator();
	}

}
