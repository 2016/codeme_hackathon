package pl.codeme.jsea.server.actions;

import javax.xml.bind.JAXBException;

import org.json.JSONException;
import org.json.JSONObject;

import pl.codeme.jsea.database.XMLParser;
import pl.codeme.jsea.database.model.Person;
import pl.codeme.jsea.server.exceptions.Exceptions;
import pl.codeme.jsea.server.exceptions.ServerException;

/** Class for action "add". It extends abstract class Action and implements execute method.
 * 
 * @author malgorzata.syska@o2.pl
 */
public class AddNewPersonAction extends Action {

	/**  It is called when an user want to use "add" action. An user has to set all data of person. 
	 * 
	 * @param data JSONObject which was sent by client
	 * @return JSONObject - response for find action
	 */
	@Override
	public JSONObject execute(JSONObject data) throws ServerException, JAXBException {

		XMLParser parser = new XMLParser("persons.xml");
		JSONObject resData = new JSONObject();
		
		Person personGivenByUser = new Person(0, data.getString("name"), data.getString("surname"),
				data.getString("phoneNumber"), data.getString("city"), data.getInt("height"));
				
		try {
			parser.addPersonToList(personGivenByUser);
			resData.put("komunikat", "Osoba została dodana.");
		} catch (JSONException e) {
			resData.put("komunikat", "Osoba nie została dodana.");
			try {
				Exceptions.PERSON_NOT_ADDED.throwException();
			} catch (ServerException f) {
				System.out.println("błąd w execute w AddAction przy dodawaniu osoby: " + e.getMessage());
			}	
		}
		return resData;
	}

}
