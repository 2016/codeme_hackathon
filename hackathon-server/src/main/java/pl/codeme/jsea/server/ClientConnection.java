package pl.codeme.jsea.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

/** Class that handles client connection with server. It runs in thread.
 * 
 * @author malgorzata.syska@o2.pl
 * @author k.kuczynska@developers.horde.pl
 * @author maciek.stepniak@gmail.com
 */
public class ClientConnection implements Runnable {

	private Socket socket;

	public ClientConnection(Socket socket) {
		this.socket = socket;
	}

	public  void run() {
		try(
			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream())); // strumień odczytywany odklienta
	        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())); // strumień wysyłanydo klienta
	        ) {
	        Protocol protocol = new Protocol();

	        String fromClient = "";
	        // reads data from client
	        while(fromClient != null) {
	        	fromClient = reader.readLine(); // reads data from client
	            String fromServer = protocol.analyst(fromClient); // analyzes client's statement
	            // sends response
	            writer.write(fromServer);
	            writer.flush();
	        }
	    } catch(IOException e) {
	            e.printStackTrace();
	    }
	}

}
