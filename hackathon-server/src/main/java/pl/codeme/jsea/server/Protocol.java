package pl.codeme.jsea.server;

import org.json.JSONObject;

import pl.codeme.jsea.server.actions.Action;

/** Class of protocol. It analyzes statements.
 * 
 * @author malgorzata.syska@o2.pl
 * @author k.kuczynska@developers.horde.pl
 * @author maciek.stepniak@gmail.com
 */
public class Protocol {

	public String analyst(String statementJSON) {
        Statement response = new Statement();
        Statement statement = new Statement(statementJSON);
        try {
            if(statement.checkCRC() && statement.getState()) {
                response.setAction(statement.getAction().getRespons());
                Action actionObject = statement.getAction().getActionInstance();
                JSONObject dataForClient = actionObject.execute(statement.getData()); // runs action and prepares response
                response.setData(dataForClient);
            }
        } catch(Exception e) {
            response.setError(e.getMessage());
        }

        return response.getStatement() + "\n";
    }

}
