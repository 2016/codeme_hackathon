package pl.codeme.jsea.server;

/** Enum class for server responses.
 * 
 * @author malgorzata.syska@o2.pl
 * @author k.kuczynska@developers.horde.pl
 * @author maciek.stepniak@gmail.com
 */
public enum Responses {
  
    FIND("find"),
    ALL("all"),
    ADD("add"),
    DEL("del");

    private String label;	// name of action
    
    /** Constructor for enum class.
     * 
     * @param label name of response
     */
    Responses(String label) {
        this.label = label;
    }

// getter
    public String getLabel() {
        return label;
    }

    /** Static method which returns Responses-type object that corresponds with String name of response.
     * 
     * @param actionLabel name of response
     * @return Response-type object
     */
    public static Responses getResponse(String actionLabel) {
        // search of a set of elements
        for(Responses item : Responses.values()) {
            // if name of response corresponds with label of response, it returns enum object
            if(item.getLabel().equals(actionLabel)) {
                return item;
            }
        }

        return null;
    }

}
