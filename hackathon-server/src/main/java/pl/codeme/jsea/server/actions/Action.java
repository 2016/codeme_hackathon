package pl.codeme.jsea.server.actions;

import javax.xml.bind.JAXBException;

import org.json.JSONObject;

import pl.codeme.jsea.server.exceptions.ServerException;

/** Abstract class for all server actions.
 * 
 * @author malgorzata.syska@o2.pl
 * @author k.kuczynska@developers.horde.pl
 * @author maciek.stepniak@gmail.com
 */
public abstract class Action {

    public abstract JSONObject execute(JSONObject data) throws ServerException, JAXBException;
}