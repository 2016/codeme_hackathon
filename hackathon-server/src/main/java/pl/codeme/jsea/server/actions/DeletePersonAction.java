package pl.codeme.jsea.server.actions;

import javax.xml.bind.JAXBException;

import org.json.JSONException;
import org.json.JSONObject;

import pl.codeme.jsea.database.XMLParser;
import pl.codeme.jsea.server.exceptions.Exceptions;
import pl.codeme.jsea.server.exceptions.ServerException;

/** Class for action "del". It extends abstract class Action and implements execute method.
 * 
 * @author malgorzata.syska@o2.pl
 */
public class DeletePersonAction extends Action {

	/**  It is called when an user want to use "del" action.  
	 * 
	 * @param data JSONObject which was sent by client
	 * @return JSONObject - response for find action
	 */
	@Override
	public JSONObject execute(JSONObject data) throws ServerException, JAXBException {

		XMLParser parser = new XMLParser("persons.xml");
		JSONObject resData = new JSONObject();
		int idOfDeletedPerson = data.getInt("id");
		
		try {
			parser.deletePersonFromList(idOfDeletedPerson);
			resData.put("komunikat", "Osoba została usunięta.");
		} catch (JSONException e) {
			resData.put("komunikat", "Osoba nie została usunięta.");
			try {
				Exceptions.PERSON_NOT_DELETED.throwException();
			} catch (ServerException f) {
				System.out.println("błąd w execute w DeleteAction przy usuwaniu osoby: " + e.getMessage());
			}
		}
		return resData;
	}

}
