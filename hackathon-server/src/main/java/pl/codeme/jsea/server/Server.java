package pl.codeme.jsea.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.naming.NamingException;


/** Servers that runs on port 6565 and accepts up to 10 simultaneous client connections.
 * 
 * @author malgorzata.syska@o2.pl
 * @author k.kuczynska@developers.horde.pl
 * @author maciek.stepniak@gmail.com
 */
public class Server {

	public Server() throws IOException, NumberFormatException, NamingException {

        boolean stop = false; // controls server operation

        ServerSocket server = new ServerSocket(6565); // initializes server on port 6565
        ExecutorService executor = Executors.newFixedThreadPool(10); // prepares pool for 10 threads of client connections

        // listen loop
        while(!stop) {
            Socket socket = server.accept(); // accepts connection with client
            executor.submit(new ClientConnection(socket)); // runs client connection in thread
        }

        executor.shutdown(); // disconnects all clients
        server.close(); // closes server
    }

    /** Method that runs server.
     * 
     * @param args arguments passed in command line
     * @throws Exception it may throw IOException, NumberFormatException or NamingException
     */
    public static void main(String[] args) throws Exception {
        new Server();
    }

}
