package pl.codeme.jsea.server.exceptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/** Own exception class that handles all possible new server exception. 
 * It uses log4j logger to log pieces of information into a ServerException.log file.
 * 
 * @author malgorzata.syska@o2.pl
 */
public class ServerException extends Exception {

    private static final long serialVersionUID = 1L;
    private static final Logger log = LogManager.getLogger(ServerException.class.getName());

    public ServerException(String msg) {
        super(msg);
        log.error(msg);
        
    }
}
