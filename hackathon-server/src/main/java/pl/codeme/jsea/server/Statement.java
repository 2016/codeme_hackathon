package pl.codeme.jsea.server;

import org.json.JSONObject;

import pl.codeme.jsea.server.actions.Actions;
import pl.codeme.jsea.server.exceptions.Exceptions;
import pl.codeme.jsea.server.exceptions.ServerException;

/** Class represents whole message send between server and client. 
 * 
 * @author k.kuczynska@developers.horde.pl
 * @author maciek.stepniak@gmail.com
 * @author malgorzata.syska@o2.pl
 */
public class Statement {

	private JSONObject statement;

	/** Empty constructor
	 */
	public Statement() {
		this(null);
	}

	/** Constructor. If you create a new instance of Statement with none argument - it creates new
	 * JSONObject and puts in it action (field), data (object), crc (field) and state (field). 
	 * If method argument isn't null - it creates a new JSONObject with given argument and returns 
	 * whole statement with it.
	 * 
	 * @param json String object
	 */
	public Statement(String json) {
		if(json == null) {
			statement = new JSONObject();
			statement.put("action", "");
			statement.put("data", new JSONObject());
			statement.put("crc", "");
			statement.put("state", 1);
		} else {
			statement = new JSONObject(json);
		}
	}

	/** It sets field "state" to 0 and puts error message to data field in statement.
	 * 
	 * @param errMsg error message that is put info data in statement
	 */
	public void setError(String errMsg) {
		statement.put("state", 0);
		getData().put("error", errMsg);
	}

	/** It sets response action. It puts "action" into a statement. 
	 * 
	 * @param action of response
	 */
	public void setAction(Responses action) {
		statement.put("action", action.getLabel());
	}

	/** It sets request action. It puts "action" into a statement. 
	 * 
	 * @param action of client request
	 */
	public void setAction(Actions action) {
		statement.put("action", action.getLabel());
	}

	/** It puts JSONObject data and (int) crc into a statement. 
	 * 
	 * @param data data in JSONObject format that you want to send in statement
	 */
	public void setData(JSONObject data) {
		int crc = this.hash(data);
		System.out.println("crc nadany : " + crc);
		statement.put("crc", crc);
		statement.put("data", data);
		System.out.println("dane ustawione w setData : " + data.toString());
	}

	/** It gets action from statement.
	 * 
	 * @return Actions object from statement
	 */
	public Actions getAction() {
		return Actions.getAction(statement.getString("action"));
	}

	/** It gets data from statement. 
	 * 
	 * @return data in JSONObject type
	 */
	public JSONObject getData() {
		return statement.getJSONObject("data");
	}
	
	/** It gets crc from statement. 
	 * 
	 * @return crc (control sum)
	 */
	public int getCrc() {
		return statement.getInt("crc");
	}

	/** It checks if control sum is correct. It gets data from statement, hashes it and compares control sum got in this way 
	 * with crc sent in statement (hashed by client). It checks if data sent in statement was changed. 
	 * 
	 * @return true if control sume is correct or false if it's not
	 * @throws ServerException -
	 */
	public boolean checkCRC() throws ServerException {
	        int crcFromStatement = this.getCrc();
	        System.out.println("crcFromStatement : " + crcFromStatement);
	        JSONObject data = this.getData();
	        System.out.println("dane : " + data.toString());
	        int crcProper = this.hash(data);
	        System.out.println("crcProper : " + crcProper);
	        if(crcFromStatement==crcProper) {
	        	System.out.println("crc ok!");
	        	return true;
	        }
	        else {
	        	Exceptions.WRONG_CRC.throwException();
	        	return false;
	        }
	}

	/** Gets statement. 
	 * 
	 * @return statement changed into String form
	 */
	public String getStatement() {

		return statement.toString();
	}

	/** Gets state of statement. State is 1 when no errors occurred, and 0 when any error was detected. 
	 * 
	 * @return true if state is 1, and false if state is 0
	 */
	public boolean getState() {
		return (statement.getInt("state") == 1) ? true : false;
	}
	
	/** It hashes data send in statement. It changes String into CharArray and adds up all characters.
	 * 
	 * @param dataFromStatement data from server in JSONObject format
	 * @return hashed value of data or 0 when length of data is 0 characters
	 */
	public int hash(JSONObject dataFromStatement) {
		String data = dataFromStatement.toString();
		System.out.println("na czym wywoluje hasha : " + data);
		char[] tab = data.toCharArray();
		int hash = 0;
		for (int i = 0;i<tab.length;i++) {
			hash = hash + tab[i];
		}
		return hash;
	}

}
