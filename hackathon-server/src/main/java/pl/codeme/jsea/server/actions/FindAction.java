package pl.codeme.jsea.server.actions;

import javax.xml.bind.JAXBException;

import org.json.JSONObject;

import pl.codeme.jsea.database.XMLParser;
import pl.codeme.jsea.database.model.Person;
import pl.codeme.jsea.server.exceptions.Exceptions;
import pl.codeme.jsea.server.exceptions.ServerException;

/** Class for action "find". It extends abstract class Action and implements execute method. 
 * 
 * @author malgorzata.syska@o2.pl
 */
public class FindAction extends Action {

	/**  It is called when an user want to use "find" action. 
	 * 
	 * @param data JSONObject which was sent by client
	 * @return JSONObject - response for find action
	 */
    @Override
    public JSONObject execute(JSONObject data) throws ServerException, JAXBException {
        JSONObject resData = new JSONObject();
        System.out.println("execute z FindAction: " + data.toString());
        int idOfWantedPerson = 0;
        
        try {
        	idOfWantedPerson = Integer.valueOf(data.getString("id"));
        	System.out.println("ID: " + idOfWantedPerson);

        	XMLParser parser = new XMLParser("persons.xml");
        	Person foundPerson = parser.findPerson(idOfWantedPerson);

			if(foundPerson !=null) {
				resData.put("osoba", foundPerson.getId()); 
				resData.put("name", foundPerson.getName());
				resData.put("surname", foundPerson.getSurname());
				resData.put("phoneNumber", foundPerson.getPhoneNumber());
				resData.put("city", foundPerson.getCity());
				resData.put("height", foundPerson.getHeight());
			} else {
				Exceptions.PERSON_NOT_FOUND.throwException();
			}
		} catch (Exception e) {
			System.out.println("błąd w execute w FindAction przy szukaniu osoby o danym ID : " + e.getMessage());
			throw e;
		}
        return resData;
    }

}
