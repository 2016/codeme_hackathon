package pl.codeme.jsea.server.exceptions;

/** Enum class that contains all new server exceptions.
 * 
 * @author malgorzata.syska@o2.pl
 */
public enum Exceptions {
	
	PERSON_NOT_FOUND("The person that you are looking for doesn't exist."),
	WRONG_CRC("There is something wrong with CRC-check."),
	PERSON_NOT_ADDED("The person that user wanted to add wasn't added."),
	PERSON_NOT_DELETED("The person that user wanted to delete wasn't deleted.");

	private String msg; // message added to exception

	/** Constructor of enum class Exceptions.
	 * 
	 * @param msg message added to exception
	 */
	Exceptions(String msg) {
	        this.msg = msg;
	}

	/** Method that throws ServerException with message.
	 * 
	 * @throws ServerException -
	 */
	public void throwException() throws ServerException {
	        throw new ServerException(msg);
	}

	/** Method that throws ServerException with added message.
	 * 
	 * @param msg message that you want to add to exception
	 * @throws ServerException -
	 */
	public void throwException(String msg) throws ServerException {
	        throw new ServerException(this.msg + " (" + msg + ")");
	}
	
	public String getMess() {
		return msg;
	}

}
