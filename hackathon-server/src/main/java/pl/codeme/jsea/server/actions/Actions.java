package pl.codeme.jsea.server.actions;

import java.lang.reflect.InvocationTargetException;
import pl.codeme.jsea.server.Responses;

/** Enum class for actions available on server. 
 * 
 * @author malgorzata.syska@o2.pl
 * @author k.kuczynska@developers.horde.pl
 * @author maciek.stepniak@gmail.com
 */
public enum Actions {

    FIND(FindAction.class, "find"),
    ALL(GetAllAction.class, "all"),
    ADD(AddNewPersonAction.class, "add"),
    DEL(DeletePersonAction.class, "del");

    private Class<?> clazz; 	// type of action

    private String label; 		// name of action

    private Responses response; // field for response to action

    /** Constructor for enum class.
     * @param clazz type of action
     * @param label name of action
     */
    Actions(Class<?> clazz, String label) {
        this.clazz = clazz;
        this.label = label;
        response = Responses.getResponse(label);
    }

    /** Method returns an instance of abstract Action class. 
     * 
     * @return instance of abstract Action class
     */
    public Action getActionInstance() {
        Action action = null;
        try {
            action = (Action)clazz.getConstructor().newInstance(); // calls a construction of Action class
        } catch (
            InstantiationException | IllegalAccessException | IllegalArgumentException | 
            InvocationTargetException | NoSuchMethodException | SecurityException e
        ) {
            e.printStackTrace();
        }

        return action;
    }

// gettery
    public String getLabel() {
        return label;
    }

    public Responses getRespons() {
        return response;
    }

    /** Static method which returns enum element that corresponds with given String name. 
     * 
     * @param strAction String name of action
     * @return if action exists, it return enum element, if not, it returns null
     */
    public static Actions getAction(String strAction) {
        // przeszukanie zbioru elementów
        for(Actions item : Actions.values()) {
            // jesli labelakcji jest równy podanej nazwie zwracany jest element enum
            if(item.getLabel().equals(strAction)) {
                return item;
            }
        }

        return null;
    }

}
