package pl.codeme.jsea.server.actions;

import java.util.List;

import javax.xml.bind.JAXBException;

import org.json.JSONObject;

import pl.codeme.jsea.database.XMLParser;
import pl.codeme.jsea.database.model.Person;
import pl.codeme.jsea.server.exceptions.ServerException;

/** Class for action "all". It extends abstract class Action and implements execute method.
 * 
 * @author malgorzata.syska@o2.pl
 */
public class GetAllAction extends Action {

	/**  It is called when client connects with server and when a list is refreshed. 
	 * 
	 * @param data JSONObject which was sent by client
	 * @return JSONObject - response for find action
	 */
    @Override
    public JSONObject execute(JSONObject data) throws ServerException, JAXBException {
        JSONObject resData = new JSONObject();
        
        XMLParser parser = new XMLParser("persons.xml");
        List<Person> allPersons = parser.getAllPersons();
        
        for (int i = 0;i<allPersons.size();i++) {
        	resData.append("osoba", allPersons.get(i).getId());
        	resData.append("name", allPersons.get(i).getName());
        	resData.append("surname", allPersons.get(i).getSurname());
        	resData.append("phoneNumber", allPersons.get(i).getPhoneNumber());
        	resData.append("city", allPersons.get(i).getCity());
        	resData.append("height", allPersons.get(i).getHeight());
        }

        return resData;
    }

}
